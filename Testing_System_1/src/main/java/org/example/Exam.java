package org.example;

import java.time.LocalDate;

public class Exam {
    int ExamID;
    String Code;
    String Title;
    CategoryQuestion CategoryID;
    int Duration;
    Account CreatorID;
    LocalDate CreateDate;
}
