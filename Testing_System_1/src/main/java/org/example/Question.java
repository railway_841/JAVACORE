package org.example;

import java.time.LocalDate;

public class Question {
    int QuestionID;
    String Content;
    CategoryQuestion CategoryID;
    TypeQuestion TypeID;
    Account CreatorID;
    LocalDate CreateDate;
}
