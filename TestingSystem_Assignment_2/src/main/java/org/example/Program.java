package org.example;

import java.time.LocalDate;

public class Program {
    public static void main(String[] args) {

        // insert data for Department

        Department dep1 = new Department();
        dep1.DepartmentID = 1;
        dep1.DepartmentName = "Sale";

        Department dep2 = new Department();
        dep2.DepartmentID = 2;
        dep2.DepartmentName = "Marketing";

        Department dep3 = new Department();
        dep3.DepartmentID = 3;
        dep3.DepartmentName = "Accounting";

        // insert data for Position
        Position pt1 = new Position();
        pt1.PositionID = 1;
        pt1.PositionName = PositionName.PM;

        Position pt2 = new Position();
        pt2.PositionID = 2;
        pt2.PositionName = PositionName.DEV;

        Position pt3 = new Position();
        pt3.PositionID = 3;
        pt3.PositionName = PositionName.TEST;

        // insert data for Account
        Account ac1 = new Account();
        ac1.AccountID = 1;
        ac1.Email = "ac1@gmail.com";
        ac1.UserName = "ac1";
        ac1.FullName = "abc";
        ac1.DepartmentID = dep1;
        ac1.PositionID = pt1;
        ac1.CreateDate = LocalDate.of(2022, 11, 10);

        Account ac2 = new Account();
        ac2.AccountID = 2;
        ac2.Email = "ac2@gmail.com";
        ac2.UserName = "ac2";
        ac2.FullName = "def";
        ac2.DepartmentID = dep2;
        ac2.PositionID = pt2;
        ac2.CreateDate = LocalDate.of(2022, 12, 12);

        Account ac3 = new Account();
        ac3.AccountID = 3;
        ac3.Email = "ac3@gmail.com";
        ac3.UserName = "ac3";
        ac3.FullName = "ghi";
        ac3.DepartmentID = dep3;
        ac3.PositionID = pt3;
        ac3.CreateDate = LocalDate.of(2022, 10, 10);

        // insert data for Group
        Group g1 = new Group();
        g1.GroupID = 1;
        g1.GroupName = "Group 1";
        g1.CreatorID = ac1;
        g1.CreateDate = LocalDate.of(2022, 11, 20);


        Group g2 = new Group();
        g2.GroupID = 2;
        g2.GroupName = "Group 2";
        g2.CreatorID = ac2;
        g2.CreateDate = LocalDate.of(2022, 12, 12);

        Group g3 = new Group();
        g3.GroupID = 3;
        g3.GroupName = "Group 3";
        g3.CreatorID = ac3;
        g3.CreateDate = LocalDate.of(2022, 10, 10);
        Group[] dsGroup1 = {g2, g3};
        ac2.groups = dsGroup1;

        Excersice1 excercise1 = new Excersice1();
        excercise1.Question1(ac2);
    }
}
