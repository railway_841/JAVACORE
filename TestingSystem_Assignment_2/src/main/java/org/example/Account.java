package org.example;

import java.time.LocalDate;

public class Account {
    public int AccountID;
    public String  Email;
    public String UserName;
    public String FullName;
    public Department DepartmentID;
    public Position PositionID;
    public LocalDate CreateDate;
    public Group[] groups;
}
